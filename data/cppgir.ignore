# generic
.*_unref
.*_free
.*_ref
.*_ref_sink

# GLib
GLib:.*:.*LOG_DOMAIN
GLib:record:Error
# deprecated
GLib:.*:g_assert_warning
GLib:.*:g_slice_.et_config
GLib:.*:g_variant_get_gtype
# too tricky with generic signature
GLib:.*:g_source_set_callback.*
# annotation problem
GLib:.*:.*g_strv_contains.*
GLib:.*:g_unichar_to_utf8
# not needed; cause trouble otherwise
#GLib:record:ByteArray
#GLib:record:Bytes
GLib:record:PtrArray
GLib:record:Array
GLib:record:S?List
GLib:record:HashTable
GLib:record:HashTableIter
GLib:record:Queue
# annotation problem
GLib:VariantType:string_scan
# conditional constants
GLib:constant:macro__has_attribute.*
GLib:constant:.*GETTEXT_DOMAIN
GLib:constant:.*WIN32.*
# not defined for C++ compiler
GLib:constant:.*C_STD_VERSION*

# GObject
GObject:record:Value
# deprecated
GObject:record:ValueArray

# GIO
# deprecated
Gio:interface:DesktopAppInfoLookup
Gio:.*:g_notification_set_urgent
Gio:.*:g_settings_list_keys
# annotation problem
Gio:virtual-method:.*ask_question
# not covered by header includes
Gio:function:g_networking_init
# external plugin module API
Gio:method:g_io_module_(load|unload)
Gio:function:g_io_module_query

# Gst
Gst:.*:.*ERROR_SYSTEM
Gst:.*:.*DebugFuncPtr

# Gtk and lower layers
#
# deprecated
GdkPixbuf:record:Pixdata.*
GdkPixbuf:bitfield:Pixdata.*
GdkPixbuf:constant:PIXBUF_MAGIC_NUMBER
GdkPixbuf:constant:PIXDATA_HEADER_LENGTH

# wrong annotation
cairo:function:image_surface_create
# repeated as GdkRectangle
cairo:record:RectangleInt

# from generated code
Pango:record:ScriptForLang

# in a header without extern C guard
Atk:function:get_major_version
Atk:function:get_minor_version
Atk:function:get_micro_version
Atk:function:get_binary_age
Atk:function:get_interface_age

# private
Gdk:method:destroy_notify
Gdk:function:synthesize_window_state

# xlib GIR does not specify header
# and including that one makes things really messy
# (due to all sorts of define's)
xlib:.*

# recent GIR describes way more than it specifies headers
# also pretty low level, so let's sidestep altogether
HarfBuzz:.*

# likewise filter out some related GtkX parts
# (should be in a separate ns btw for good measure)
Gtk:class:Plug
Gtk:class:Socket
# private
Gtk:method:gtk_widget_path_iter_add_qclass
# missing in summary header gtk-a11y.h
Gtk:class:HeaderBarAccessible
Gtk:class:FileChooserWidgetAccessible

# signal emission method wrappers
# undocumented; and should not be wrapped either
# (with some problematic parameter types)
Soup:method:soup_message_content_sniffed
Soup:method:soup_message_wrote_chunk
Soup:method:soup_message_wrote_headers
Soup:method:soup_message_wrote_body
Soup:method:soup_message_wrote_informational
Soup:method:soup_message_got_chunk
Soup:method:soup_message_got_headers
Soup:method:soup_message_got_body
Soup:method:soup_message_got_informational
Soup:method:soup_message_starting
Soup:method:soup_message_restarted
Soup:method:soup_message_finished
# deprecated interface since 13 years
Soup:interface:PasswordManager
# private
Soup:function:soup_get_resource
# unstable API
Soup:class:Requester$
Soup:method:soup_websocket_extension.*
Soup:enumeration:RequesterError
